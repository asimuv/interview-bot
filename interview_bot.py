#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
from timeit import default_timer as timer
from subprocess import call


class Bot(object):


    def __init__(self, *args, **kwargs):
        self.settings = {}
        
    
    def load_settings(self):
        """
        loads the settings
        """
        settings_path = '{0}{1}{2}'.format(os.getcwd(), os.path.sep, 'SETTINGS')
        
        with open(settings_path, 'r') as f:
            raw_settings = f.readlines()
        
        for line in raw_settings:
            self.settings[self.clean_line(line)[0]] = self.clean_line(line)[1]
    
        
    def load_script(self):
        """
        loads the interview script into memory
        """
        script_path = '{0}{1}{2}'.format(os.getcwd(), os.path.sep, self.settings.get('script'))       
        
        with open(script_path, 'r') as f:
            self.script = f.readlines()


    def set_voice(self):
        """
        sets say command -v option
        """
        if self.settings.get('voice'):
            self.voice = self.settings.get('voice')
        else:
            self.voice = 'Agnes'


    def setup(self):
        """
        Loads settings
        Loads script
        Sets voice
        """
        self.load_settings()
        self.load_script()
        self.set_voice()


    def clean_line(self, line):
        """
        convenience utility to format and clean data
        from SCRIPT and SETTINGS files
        """
        if len(line) < 2:
            return None, None

        current_line = line.split('|')
        return  current_line[0].strip(), current_line[1].strip()


    def say(self, something):
        """
        calls subprocess say
        sleeps for one second
        after say command exits
        to avoid the script to
        sound like one long sentence
        """
        call(['say', '-v', self.voice, something])
        time.sleep(1)
        return True


    def time_elapsed(self, start, end):
        """
        returns time elapsed between
        two timer() objects
        """
        return end - start


    def time_pprint(self, start, end):
        """
        Pretty prints time
        into a human redable format
        hh:mm:ss
        """
        minutes, seconds = divmod(self.time_elapsed(start, end), 60)
        hours, minutes = divmod(minutes, 60)
        return "%02d:%02d:%02d" % (hours, minutes, seconds)


    def run(self, **kwargs):
        """
        Start the interview
        
        Accepts keyword argument test_option
        to ease unit testing. Allows to
        pass options without asking
        for user input.
        """
        start_time = timer()
        
        option = None
        
        for line in self.script:
            
                        
            content_type, content = self.clean_line(line)
            
            if content_type == 'statement':
                self.say(content)
            
            elif content_type == 'question':
                self.say(content)
                
                while option not in ('n', 'q'):
                    
                    if kwargs.get('test_option'):
                        option = kwargs.get('test_option')
                    else:
                        option = raw_input('Options:  (r)epeat, (n)ext, (q)uit: ')
                    
                    if option == 'r':
                        self.say(content)
                    
                    if option == 'n':
                        # reset it option to None to avoid
                        # looping over script without stopping
                        option = None
                        break

                    if option == 'q':
                        print 'You quit the interview. Goodbye!'
                        return False
            
            else:
                continue
        
        print 'Total interview time was:', self.time_pprint(start_time, timer())
        
        return True


if __name__ == '__main__':
    b = Bot()
    b.setup()
    b.run()