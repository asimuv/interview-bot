***************************************************************
*************************asimuv.com****************************
***************************************************************
***************************************************************



 _       _                  _                 _           _   
(_)     | |                (_)               | |         | |  
 _ _ __ | |_ ___ _ ____   ___  _____      __ | |__   ___ | |_ 
| | '_ \| __/ _ \ '__\ \ / / |/ _ \ \ /\ / / | '_ \ / _ \| __|
| | | | | ||  __/ |   \ V /| |  __/\ V  V /  | |_) | (_) | |_ 
|_|_| |_|\__\___|_|    \_/ |_|\___| \_/\_/   |_.__/ \___/ \__|




			              .__                     
			_____    _____|__| _____  __ _____  __
			\__  \  /  ___/  |/     \|  |  \  \/ /
			 / __ \_\___ \|  |  Y Y  \  |  /\   / 
			(____  /____  >__|__|_|  /____/  \_/  
			     \/     \/         \/             


***************************************************************
***************************************************************
**********************Do*The*Impossible************************
***************************************************************



****************************************
# What is interview bot?
****************************************

Interview bot is a python 2.7 script
that uses the OSX `say` command to
help you practice for interviews.

It was developed and tested for the English language.

License: MIT.

See LICENSE file for details.

****************************************
# Requirements
****************************************

This was developed on OSX 10.10.5.
No other version has been tested.
Should work OK on similar OSX versions
as long as they have python 2.7 available.

No third party python dependencies required.

****************************************
# Features
****************************************

- You can define your own interview script.
- You can change the voice of the bot.
- It will track how long the practice interview take.
- Quit at any moment.
- Repeat the current question (good for practicing hard to understand interviewers).
- Skip questions.

****************************************
# Instructions
****************************************


////////////////////////
1. Settings
////////////////////////


1a. There are two settings available.
	- `script` : Required. Name of the script that the bot will use for the interview

	- `voice` : Optional. Voice that will be used for the interview. OSX provides various options outlined below.

1b. How to define settings:

This is the content of the default settings file:

script| SCRIPT

Where script is the settings key and SCRIPT is the settings value.
SCRIPT is a case senstitive filename.


1c. How to define a different script:

Simply change the value of the `script` setting like so:

script| YOUR-SCRIPT-NAME

1d. Optional settings

You can change the voice of the bot. Alternate voices available 
are listed in the section *Optional Voices*.

To change the voice type add the following setting to the SETTINGS file:

voice| Your-Voice-Choice

For example:

voice| bruce

The voice name is *not* case sensitive.


////////////////////////
2. Running the bot.
////////////////////////


2a. You run it like any other python script: python interview_bot.py
    It does not require anything special.


2b. The bot will say things outlined in the script
    and will also print information on the terminal.

2c. The bot is controlled through the terminal
    by passing an command. The available commands are:
	
	(r)epeat - Repeats the last question covered.
	(n)ext - Skips to the next question.
	(q)uit - Quits the interview.
	
	Note: You only need to type the first letter of the command!

2d. Once the interview is finished the bot will
    print the interview running time in the terminal.

****************************************
# How to define your own script
****************************************

The default script is simple because interviews vary in complexity.
Defining a new script is easy.

A script has two data types available:

- A `statement` type. Which is a sentence the will say 
 without pausing to give you time to answer.

- A `question` type. Which is an interview question. 
  The bot will pause until you answer and signal it to continue.

*Important* Every `statement` and `question` must be on their own
            separate line.
			
Good:

statement| Hello, world.
question| What is your name?

Bad:

statement|Hello, world. question|What is your name?


$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

* Using Paragraphs *

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Its best if you define each sentence as an individual statement.
The bot adds a one second pause after each statement.
Putting multiple sentences in one sentece will sound like
one really long run-on sentence.


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

## Understanding the default script

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

This is the default script:

statement| Welcome to interview bot.
question| What is a hash table?
statement| The interview is now over.
statement| Goodbye!


The bot will loop over the contents.
Passing each line to the `say` command.
If its a `statement` type, the bot will pause
for a second before moving on to the next.


The bot handles empty lines to aide in script legibility.

This script:

statement| Welcome to interview bot.
statement| Goodbye!

Will work in the same way as this script:

statement| Welcome to interview bot.

statement| Goodbye!


Here is an empty script template you can use:

statement|
statement|

question|
question|

statement|
statement|

****************************************
# Optional Voices
****************************************

You can change the voice of the bot to any of the ones available on OSX.

Available voice options are:

Female Voices

	Agnes – relatively natural female voice
	Kathy – sounds like a computer, kind of like the computer voice
	Princess – kind of sounds like an old lady
	Vicki – most natural, kind of whispery
	Victoria – computery sounding

Male Voices

	Bruce – most natural male voice
	Fred – sounds like a computer
	Junior – sounds like a girl
	Ralph – deep voice

Novelty Voices

	Albert – funny weak whispery high pitched voice
	Bad News – very slow low-pitched voice (put “Bad News” in quotes)
	Bahh – choppy whispery weak voice
	Bells – Melodic bells within voice, relatively slow paced
	Boing – Mid-pitched typical computer voice with “boing” sound in each syllable
	Bubbles – Whispery voice with bubble sounds in each syllable
	Cellos – Melodic voice with cello voice, each syllable is a successive note in a simple tune
	Deranged – Old man-ish voice, think Grandpa Simpson
	Good News – Says words to the tune of “Pomp and Circumstance” or other tunes
	Hysterical – Laughing/chuckling man voice (funny but hard to understand)
	Pipe Organ – Low “pipe organ” melodic voice
	Trinoids – Monotonous mid-pitched computer voice
	Whisper – Whispery, kind of desperate sounding, sounds like it is having trouble breathing
	Zarvox – Monotonous mid-pitched computer voice


****************************************
# Legal stuff
****************************************

Copyright 2016 Pablo Rivera
contact: pr@asimuv.com

This software is licences under the MIT License.
You should have received a copy of the license 
with the software.