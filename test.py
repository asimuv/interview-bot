#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from interview_bot import Bot


class PrimesTestCase(unittest.TestCase):
    """Tests for Bot class"""


    def setUp(self):
        self.bot = Bot()


    def test_settings_are_loaded(self):
        """
        tests if SETTINGS file is found
        in the same directory as script
        and it gets properly loaded
        """
        self.bot.load_settings()
        self.assertTrue(self.bot.settings['script'])


    def test_script_is_loaded(self):
        """
        tests if the SCRIPT file is found
        in the same directory as script
        and it gets properly loaded
        """
        self.bot.load_settings()
        self.bot.load_script()
        self.assertTrue(self.bot.script)


    def test_bot_voice_is_set(self):
        """
        tests if the SCRIPT file is found
        in the same directory as script
        and it gets properly loaded
        """
        self.bot.load_settings()
        self.bot.set_voice()
        self.assertTrue(self.bot.voice)


    def test_setup(self):
        """
        Test setup utility method.
        This method calls others.
        This tests covers that everything is
        setup as required.
        """
        self.bot.setup()
        self.assertTrue(self.bot.settings['script'])
        self.assertTrue(self.bot.script)
        self.assertTrue(self.bot.voice)


    def test_clean_line(self):
        self.line = 'statement| This is a test line'
        self.assertTrue(self.bot.clean_line(self.line))
        self.assertEqual(self.bot.clean_line(self.line)[0], 'statement')
        self.assertEqual(self.bot.clean_line(self.line)[1], 'This is a test line')


    def test_say(self):
        """
        This test does not cover
        the actual subprocess call.
        It tests that the method does
        not error out and returns True.

        Calls say OSX command.
        A passign test means you will
        listen the mac use the say command.
        """
        self.bot.load_settings()
        self.bot.load_script()
        self.bot.set_voice()
        self.assertTrue(self.bot.say('Test say unit test passes'))


    def test_time_elapsed_returns_float(self):
        result = self.bot.time_elapsed(12.8, 45.3)
        self.assertEqual(float, type(result))


    def test_time_pprint_returns_correctly_formatted_string(self):
        result = self.bot.time_pprint(45.3, 128.12)
        self.assertEqual(result, '00:01:22')


    def test_run_quits_if_q_is_passed_as_test_option(self):
        self.bot.setup()
        self.assertFalse(self.bot.run(test_option='q'))


    def test_run_finishes_interview_if_n_is_passed_as_test_option(self):
        self.bot.setup()
        self.assertTrue(self.bot.run(test_option='n'))


if __name__ == '__main__':
    unittest.main()